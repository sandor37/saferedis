﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SafeRedis.Interfaces;

namespace SafeRedis.Services
{
    public class RedisCacheReader
    {
        private IRedisCacheConfiguration _configuration { get; set; }

        public RedisCacheReader():this(null) { }
        public RedisCacheReader(IRedisCacheConfiguration config)
        {
            _configuration = config;
        }
    }
}
